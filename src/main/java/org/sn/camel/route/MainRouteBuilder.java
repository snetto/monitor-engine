package org.sn.camel.route;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;

import com.calypso.tk.service.DSConnection;
import com.calypso.tk.util.ConnectionUtil;

/**
 * 
 * @author : Silvio Netto (snetto@cpqi.com)
 * 
 */
public class MainRouteBuilder extends RouteBuilder {

    static DSConnection dsConnection;

    public static void main(String[] args) throws Exception {
	dsConnection = ConnectionUtil.connect("calypso_user", "calypso",
		"Check Engines", "development");
	Main.main(args);
    }

    @Override
    public void configure() throws Exception {
	from("timer://scheduled?fixedRate=true&delay=0&period=10000").to(
		"direct:check.engines");

	from("direct:check.engines").process(new Processor() {

	    @Override
	    public void process(Exchange exchange) throws Exception {

		Collection apps = dsConnection.getApplicationNames();
		List<String> engines = new ArrayList<String>();
		engines.add("MessageEngine");
		engines.add("SenderEngine");
		for (String engineName : engines) {
		    if (!apps.contains(engineName)) {
			System.out.println("Start " + engineName);
			Runtime rt = Runtime.getRuntime();
			final Process p = rt
				.exec("C:/BTG/calypsorel130007SP2/bin/runjava.bat com.calypso.apps.startup.Start"
					+ engineName
					+ " -user calypso_user -password calypso -env development -nologingui");

			System.out
				.println("Engine " + engineName + " started!");
		    }
		}
	    }
	});
    }
}